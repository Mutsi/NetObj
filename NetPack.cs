﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetObj
{
	[ProtoContract]
	public class NetPack
	{
		[ProtoMember(1)]
		public byte PackID;
		[ProtoMember(2)]
		public byte[] content;
		public delegate void Execute(NetPack pack);
		public Execute ExcecuteFunc;
		[ProtoMember(3)]
		public byte isReliable;
		[ProtoMember(4)]
		public Guid Guid;
		[ProtoMember(5)]
		public uint SequenceNr;

		[ProtoIgnore]
		public UDPConnection connection;
		[ProtoIgnore]
		public DateTime SentAt;

		public byte[] SerializePackage()
		{
			MemoryStream stream = new MemoryStream();
			stream.Position = 0;
			stream.Flush();
			Serializer.SerializeWithLengthPrefix<NetPack>(stream, this, PrefixStyle.Base128);
			stream.Close();

			return stream.ToArray();
		}
	}
}
