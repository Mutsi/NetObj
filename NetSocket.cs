﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NetObj
{

	public class ClientConnectedArgs : EventArgs
	{
		public UDPConnection connection;
	}


	public class UDPConnection
	{
		public List<NetPack> ExecutionQueue = new List<NetPack>();
		public Dictionary<Guid, NetPack> ReliableCache = new Dictionary<Guid, NetPack>();
		public float AvgLatency;
		public float LastLatency;


		public UDPConnection(IPEndPoint endpoint)
		{
			this.endpoint = endpoint;
			lastUpdate = DateTime.Now;
			sequencenr = 0;
			localSequenceNr = 0;
		}

		public void Update()
		{
			lastUpdate = DateTime.Now;
		}

		public IPEndPoint endpoint;
		public uint sequencenr;
		public uint localSequenceNr;
		public DateTime lastUpdate;
	}

	public class NetSocket
	{
		protected bool isActive = true;
		protected Thread thread;

		public Dictionary<string, UDPConnection> UDPConnections = new Dictionary<string, UDPConnection>();
		public EventHandler<ClientConnectedArgs> NewConnectionEvent;

		Random random = new Random();
		private List<Guid> ackCache = new List<Guid>();

		public virtual void ReadData(byte[] data, UDPConnection client)
		{
			MemoryStream stream = new MemoryStream(data);
			NetPack pack = Serializer.DeserializeWithLengthPrefix<NetPack>(stream, PrefixStyle.Base128);
			pack.connection = client;

			client.Update();

			if (pack.PackID == 0xff) //Else check if it is an ack and remove it from reliableCache
			{

				//Console.WriteLine(string.Format("[{0}] : {1} ", pack.Guid.ToString(), "Acknowledged"));
				lock (client.ReliableCache)
				{
					if (client.ReliableCache.ContainsKey(pack.Guid))
					{
						TimeSpan latency = DateTime.Now - client.ReliableCache[pack.Guid].SentAt;
						client.LastLatency = (float)latency.TotalMilliseconds;
						client.AvgLatency = client.AvgLatency * 0.9f + client.LastLatency * 0.1f;

						uint seqNrConfirmed = client.ReliableCache[pack.Guid].SequenceNr;


						client.ReliableCache.Remove(pack.Guid);
						/*for (int i = 0; i < client.ReliableCache.Count; i++)
						{
							if (client.ReliableCache.Values.ElementAt(i).SequenceNr <= seqNrConfirmed)
								client.ReliableCache.Remove(client.ReliableCache.Values.ElementAt(i).Guid);
						}*/
						//client.ReliableCache.RemoveAll(client.ReliableCache.SingleOrDefault(s => s.Value.SequenceNr <= seqNrConfirmed).Key);
					}
				}
			}

			if (ExecutionMap.Map.ContainsKey(pack.PackID) && !ackCache.Contains(pack.Guid))
			{
				if (pack.isReliable == 0x1)
				{
					ackCache.Add(pack.Guid);

					//For initial connection we take the sequence number from the remote
					if (client.sequencenr == 0)
						client.sequencenr = pack.SequenceNr;

					if (pack.SequenceNr == client.sequencenr)
					{
						pack.ExcecuteFunc = ExecutionMap.Map[pack.PackID];
						pack.ExcecuteFunc(pack);
						client.sequencenr++;
					}
					else if (pack.SequenceNr > client.sequencenr)
					{
						//Add to execute queue
						pack.ExcecuteFunc = ExecutionMap.Map[pack.PackID];
						lock (client.ExecutionQueue)
						{
							client.ExecutionQueue.Add(pack);
						}
					}
				}
				else
				{
					pack.ExcecuteFunc = ExecutionMap.Map[pack.PackID];
					pack.ExcecuteFunc(pack);
				}


				RunExecQueue();
			}

			//if we receive a reliable we have to send back a ack
			if (pack.isReliable == 0x1)
			{
				//Add ack to ack cache so itll be send back automatically up until ack is received
				SendAck(pack);
			}
		}

		public void RunExecQueue()
		{
			foreach (var item in UDPConnections)
			{
				lock (item.Value.ExecutionQueue)
				{
					item.Value.ExecutionQueue = item.Value.ExecutionQueue.OrderBy(o => o.SequenceNr).ToList();
					while (item.Value.ExecutionQueue.Count > 0 && item.Value.ExecutionQueue.First().SequenceNr == item.Value.sequencenr)
					{
						item.Value.ExecutionQueue.First().ExcecuteFunc(item.Value.ExecutionQueue.First());
						item.Value.ExecutionQueue.RemoveAt(0);
						item.Value.sequencenr++;
					}
				}
			}

		}

		int reliableWait = 0;
		int reliableMaxWait = 3;
		public void ResendReliable()
		{
			foreach (UDPConnection client in UDPConnections.Values)
			{
				reliableWait++;
				if (reliableWait > reliableMaxWait)
				{
					reliableWait = 0;
					lock (client.ReliableCache)
					{
						client.ReliableCache = client.ReliableCache.OrderBy(o => o.Value.SequenceNr).ToDictionary(o => o.Key, k => k.Value);
						if (client.ReliableCache.Count > 0)
							SendPack(client.ReliableCache.Values.First());

						/*for (int i = 0; i < client.ReliableCache.Count; i++)
						{
							SendPack(client.ReliableCache.Values.ElementAt(i));
						}*/
					}
				}
			}
			/**/
		}

		public void SendAck(NetPack pack)
		{
			NetPack ackpack = new NetPack();
			ackpack.isReliable = 0x0;
			ackpack.Guid = pack.Guid;
			ackpack.PackID = 0xff;
			ackpack.connection = pack.connection;

			SendPack(ackpack);
		}

		public virtual void SendPack(NetPack pack)
		{
			if (pack.PackID != 0xff && pack.Guid == Guid.Empty)
				pack.Guid = Guid.NewGuid();
			if (pack.isReliable == 0x1 && !pack.connection.ReliableCache.ContainsKey(pack.Guid))
			{
				pack.SentAt = DateTime.Now;
				pack.SequenceNr = pack.connection.localSequenceNr;
				lock (pack.connection.ReliableCache)
				{
					pack.connection.ReliableCache.Add(pack.Guid, pack);
				}
				pack.connection.localSequenceNr++;
			}
		}

		public virtual void Shutdown()
		{
			isActive = false;
			thread.Join();
			thread.Abort();
		}
	}
}
